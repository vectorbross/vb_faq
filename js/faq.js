(function($, window, document, Drupal) {
	$(function() {
		$('.view-vb-faq .panel-collapse').on({
			'show.bs.collapse': function() {
				$(this).parent('.panel').addClass('panel-open');
			},
			'hide.bs.collapse': function() {
				$(this).parent('.panel').removeClass('panel-open');
			}
		});
	});

	$(".accordion-search--input").on('keyup', function() {
		var filter = $(this).val();
		var regex = new RegExp(filter, "i");
		var noresult = true;

		// Hide paragraph (title) when no results are present
		$('.faq-item--teaser').each(function() {
			if ($(this).text().search(regex) < 0) {
				$(this).fadeOut();
			}
			else if(filter == '') {
				$(this).fadeIn();
				noresult = false;
			}
			else {
				$(this).fadeIn();
				noresult = false;
			}
		});

		if(noresult == true) {
			$('.faq-no-results').fadeIn();
		}
		else {
			$('.faq-no-results').fadeOut();
		}

		// Hide individual panels without results
		$('.faq-item--teaser').each(function() {
			if ($(this).text().search(regex) < 0) {
				$(this).fadeOut();
				$(this).collapse("hide");
			} else {
				$(this).fadeIn();
				$(this).removeHighlight();
				$(this).highlight(filter);
				if(filter.length > 0) {
					$(this).find('.anwser').collapse("show");
				}
				else {
					$(this).find('.anwser').collapse("hide");
				}
			}
		});
	});
})(jQuery, window, document, Drupal);