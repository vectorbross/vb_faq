<?php

namespace Drupal\vb_faq\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;

/**
 * Provides a 'FAQ Overview' block.
 *
 * @Block(
 *   id = "faq_overview_block",
 *   admin_label = @Translation("FAQ Overview Block"),
 *   category = @Translation("Vector BROSS"),
 * )
 */
class FaqOverviewBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'faq_category' => '',
      'textual_search' => FALSE,
    ];
  }

  /**
   * Returns the 'Faq Category' options.
   *
   * @return array
   */
  protected function getFaqCategoryOptions() {
    $options = [
      '' => $this->t('All'),
    ];

    $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
      ->loadTree('faq_categories', 0, 1, TRUE);

    foreach ($tree as $term) {
      $options[$term->id()] = $term->getName();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['faq_category'] = [
      '#type' => 'select',
      '#title' => $this->t('FAQ Category'),
      '#options' => $this->getFaqCategoryOptions(),
      '#default_value' => $config['faq_category'],
    ];

    $form['textual_search'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable textual search'),
      '#default_value' => $config['textual_search'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['faq_category'] = $values['faq_category'];
    $this->configuration['textual_search'] = $values['textual_search'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $view_id = 'faq';
    $enable_search_bar = FALSE;

    $faq_category = $this->getConfiguration()['faq_category'];
    $textual_search = $this->getConfiguration()['textual_search'];

    $components = [];

    if (isset($textual_search) && $textual_search == '1') {
      $enable_search_bar = TRUE;

      $components['search'] = [
        '#type' => 'textfield',
        '#placeholder' => t('Search by keywords'),
        '#attributes' => [
          'class' => ['accordion-search--input'],
          'aria-label' => ['FAQ search'],
        ],
        '#suffix' => '<div class="no-result--wrapper"></div>',
      ];
    }

    if (isset($faq_category) && $faq_category != '') {
      // Show Category view.
      $display_id = 'faq_overview_by_category_block';
      $args = [$faq_category];
    }
    else {
      // Show full FAQ overview.
      $enable_search_bar = TRUE;
      $display_id = 'faq_overview_block';
      $args = [];

      // Exposed filter should be rendered separately. Exposed as block.
      $view = Views::getView($view_id);
      if (!empty($view)) {
        $view->setDisplay($display_id);
        $view->initHandlers();
        $form_state = (new FormState())
          ->setStorage([
            'view' => $view,
            'display' => &$view->display_handler->display,
            'rerender' => TRUE,
          ])
          ->setMethod('get')
          ->setAlwaysProcess()
          ->disableRedirect();
        $form_state->set('rerender', NULL);

        $components['exposed_filters'] = \Drupal::formBuilder()->buildForm('\Drupal\views\Form\ViewsExposedForm', $form_state);
      }
    }

    $components['view'] = [
      '#type' => 'view',
      '#name' => $view_id,
      '#display_id' => $display_id,
      '#arguments' => $args,
    ];

    $build = [
      '#theme' => 'faq_overview_block',
      '#enable_search_bar' => $enable_search_bar,
      '#components' => $components,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
